+++
title = "About Me"
slug = "about"
+++

¿Por qué quieres trabajar conmigo?

Mi nombre es Luis. Tengo las siguientes cualidades:

* Me gusta viajar.
* Soy extremadamente leal a mis amigos y clientes.
* Cumplo con los plazos estipulados.
* Sysadmin de nacimiento, desarrollador en crecimiento.

## Hoy

DevOps & Cloud Architect de PreyProject. Responsable de satisfacer la continua demanda de todo el equipo de tecnología, para un desarrollo continuo. Con más de 10 años de experiencia en tecnologías opensource.

Thanks for reading!
