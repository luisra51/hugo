+++
title = "About Me"
slug = "about"
+++

Why would you want to work with me?

My name is Luis. I have the following qualities:

* I like travel
* I am extremely loyal to my friends and customers.
* I comply with the stipulated deadlines
* Sysadmin by birth, growing developer



## Now

DevOps & Cloud Architect of PreyProject. Responsible for meeting the continuous demand of the entire technology team, for continuous development. With more than 10 years of experience in opensource technologies. Sysadmin by birth, growing developer

Thanks for reading!
